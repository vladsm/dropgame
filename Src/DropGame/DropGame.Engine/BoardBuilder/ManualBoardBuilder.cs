using System;
using System.Collections.Generic;

namespace DropGame
{
	//public class ManualBoardBuilder : IBoardBuilder
	//{
	//	private int _xSize;
	//	private int _ySize;
	//	private readonly List<PieceLocation> _pieces = new List<PieceLocation>();

	//	public void SetSize(int xSize, int ySize)
	//	{
	//		if (xSize < 1) throw new ArgumentOutOfRangeException("xSize");
	//		if (ySize < 1) throw new ArgumentOutOfRangeException("ySize");
	//		_ySize = ySize;
	//		_xSize = xSize;
	//	}

	//	public void AddPiece(int x, int y, IBoardItem boardItem)
	//	{
	//		if (x < 0) throw new ArgumentOutOfRangeException("x");
	//		if (y < 0) throw new ArgumentOutOfRangeException("y");
	//		if (boardItem == null) throw new ArgumentNullException("boardItem");

	//		_pieces.Add(new PieceLocation(x, y, boardItem));
	//	}

	//	public BoardState CreateBoard()
	//	{
	//		if (_xSize < 1 || _ySize < 1) throw new InvalidOperationException("The board size is not specified.");

	//		var pieces = new IBoardItem[_xSize, _ySize];
	//		var blankPiece = new BlankBoardItem();
	//		for (int x = 0; x < _xSize; ++x)
	//		{
	//			for (int y = 0; y < _ySize; ++y)
	//			{
	//				if (pieces[x, y] == null)
	//				{
	//					pieces[x, y] = blankPiece;
	//				}
	//			}
	//		}

	//		foreach (var pieceLocation in _pieces)
	//		{
	//			if (pieceLocation.X < _xSize && pieceLocation.Y < _ySize)
	//			{
	//				pieces[pieceLocation.X, pieceLocation.Y] = pieceLocation.BoardItem;
	//			}
	//		}

	//		return new BoardState(pieces);
	//	}


	//	private class PieceLocation
	//	{
	//		public int X { get; private set; }
	//		public int Y { get; private set; }
	//		public IBoardItem BoardItem { get; private set; }

	//		public PieceLocation(int x, int y, IBoardItem boardItem)
	//		{
	//			X = x;
	//			Y = y;
	//			BoardItem = boardItem;
	//		}
	//	}
	//}
}
