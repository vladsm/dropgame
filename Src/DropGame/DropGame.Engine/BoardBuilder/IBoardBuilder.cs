namespace DropGame
{
	public interface IBoardBuilder
	{
		BoardState CreateBoard();
	}
}
