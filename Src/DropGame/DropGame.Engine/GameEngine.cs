﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DropGame
{
	public class GameEngine
	{
		private readonly IServiceLocator _serviceLocator;

		public GameEngine(IServiceLocator serviceLocator)
		{
			_serviceLocator = serviceLocator;
		}

		internal IServiceLocator ServiceLocator
		{
			get { return _serviceLocator; }
		}

		public SceneEngine CreateSceneScreen(
			[NotNull] IEnumerable<IPlayer> players,
			[NotNull] BoardState board,
			[NotNull] ISceneHumanInput humanInput
			)
		{
			if (players == null) throw new ArgumentNullException("players");
			if (board == null) throw new ArgumentNullException("board");
			if (humanInput == null) throw new ArgumentNullException("humanInput");

			IEnumerable<HumanPlayerEngine> playerEngines = players.Select(
				player => new HumanPlayerEngine(
					Enumerable.
						Range(0, board.ColumnsCount).
						Select(i => new SimplePlayerPiecesStack(player, new[] {new OrdinalPlayerPiece(player)})).
						Cast<IPlayerPiecesStack>().
						ToArray(),
					humanInput
					)
				);
			
			return new SceneEngine(
				board,
				new OrdinalPlayersRotator(playerEngines.Cast<PlayerEngine>()),
				this
				);
		}
	}
}
