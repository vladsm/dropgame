using System;
using System.Collections.Generic;

using DropGame.Animation;

namespace DropGame
{
	public abstract class ScreenEngine
	{
		private readonly GameEngine _game;
		private readonly ScreenAnimationManager _animationManager;

		protected ScreenEngine([NotNull] GameEngine game)
		{
			if (game == null) throw new ArgumentNullException("game");
			_game = game;
			_animationManager = new ScreenAnimationManager();
		}

		protected GameEngine Game
		{
			get { return _game; }
		}

		protected internal IServiceLocator ServiceLocator
		{
			get { return _game.ServiceLocator; }
		}


		public IEnumerable<IAnimation> ActiveAnimations
		{
			get { return _animationManager.ActiveAnimations; }
		}


		internal IAnimation AttachAnimation(IAnimation animation)
		{
			return new ScreenAttachedAnimation(_animationManager, animation);
		}

		internal IAnimation AttachAnimation<TAnimation>(
			[CanBeNull] Action<TAnimation> setupAnimation,
			out TAnimation originalAnimation
			)
			where TAnimation : IAnimation
		{
			originalAnimation = ServiceLocator.CreateService<TAnimation>();
			if (setupAnimation != null)
			{
				setupAnimation(originalAnimation);
			}
			return AttachAnimation(originalAnimation);
		}

		internal IAnimation AttachAnimation<TAnimation>([CanBeNull] Action<TAnimation> setupAnimation)
			where TAnimation : IAnimation
		{
			TAnimation originalAnimation;
			return AttachAnimation(setupAnimation, out originalAnimation);
		}

		internal IAnimation AttachAnimation<TAnimation>(out TAnimation originalAnimation)
			where TAnimation : IAnimation
		{
			return AttachAnimation(null, out originalAnimation);
		}

		internal IAnimation AttachAnimation<TAnimation>()
			where TAnimation : IAnimation
		{
			return AttachAnimation<TAnimation>(null);
		}
	}
}
