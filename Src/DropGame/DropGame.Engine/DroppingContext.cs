﻿using System;

using DropGame.Animation;

namespace DropGame
{
	public class DroppingContext
	{
		[NotNull]
		public PlayerActionContext PlayerActionContext { get; private set; }
		
		[NotNull]
		public IPlayerPiece Piece { get; private set; }

		public BoardPosition FromPosition { get; set; }

		public BoardPosition ToPosition { get; set; }

		public SceneEngine SceneEngine
		{
			get { return PlayerActionContext.SceneEngine; }
		}

		public AnimationQueue Animations
		{
			get { return PlayerActionContext.Animations; }
		}

		public DroppingContext(
			[NotNull] IPlayerPiece piece,
			[NotNull] PlayerActionContext playerActionContext
			)
		{
			if (piece == null) throw new ArgumentNullException("piece");
			if (playerActionContext == null) throw new ArgumentNullException("playerActionContext");
			Piece = piece;
			PlayerActionContext = playerActionContext;
		}

		public DroppingContext([NotNull] DroppingContext droppingContext)
		{
			if (droppingContext == null) throw new ArgumentNullException("droppingContext");

			PlayerActionContext = droppingContext.PlayerActionContext;
			Piece = droppingContext.Piece;
			FromPosition = droppingContext.FromPosition;
			ToPosition = droppingContext.ToPosition;
		}

		public DroppingContext(
			[NotNull] DroppingContext droppingContext,
			BoardPosition fromPosition,
			BoardPosition toPosition
			) :
			this(droppingContext)
		{
			FromPosition = fromPosition;
			ToPosition = toPosition;
		}
	}
}
