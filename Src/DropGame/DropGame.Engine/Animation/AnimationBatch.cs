﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DropGame.Animation
{
	public class AnimationBatch : IAnimation
	{
		private readonly List<IAnimation> _animationsToWait;
		private readonly List<IAnimation> _backgroundAnimations;

		public AnimationBatch(
			[NotNull] IEnumerable<IAnimation> animationsToWait,
			[NotNull] IEnumerable<IAnimation> backgroundAnimations
			)
		{
			if (animationsToWait == null) throw new ArgumentNullException("animationsToWait");
			if (backgroundAnimations == null) throw new ArgumentNullException("backgroundAnimations");

			_animationsToWait = new List<IAnimation>(animationsToWait);
			if (!_animationsToWait.Any()) throw new ArgumentException("There are no animations to wait.", "animationsToWait");
			
			_backgroundAnimations = new List<IAnimation>(backgroundAnimations);
		}

		public void Play(Action completeCallback)
		{
			if (completeCallback == null) throw new ArgumentNullException("completeCallback");

			foreach (var animation in _backgroundAnimations)
			{
				animation.Play(delegate { });
			}

			foreach (var animation in _animationsToWait)
			{
				IAnimation animationForClosure = animation;
				animation.Play(() => PlayComplete(animationForClosure, completeCallback));
			}
		}

		private void PlayComplete(IAnimation animation, Action completeCallback)
		{
			_animationsToWait.Remove(animation);
			if (!_animationsToWait.Any())
			{
				completeCallback();
			}
		}
	}
}
