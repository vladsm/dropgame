﻿using System;

namespace DropGame.Animation
{
	public class ScreenAttachedAnimation : IAnimation
	{
		private readonly ScreenAnimationManager _animationManager;
		private readonly IAnimation _internalAnimation;

		public ScreenAttachedAnimation(
			[NotNull] ScreenAnimationManager animationManager,
			[NotNull] IAnimation internalAnimation
			)
		{
			if (animationManager == null) throw new ArgumentNullException("animationManager");
			if (internalAnimation == null) throw new ArgumentNullException("internalAnimation");
			_animationManager = animationManager;
			_internalAnimation = internalAnimation;
		}

		public void Play(Action completeCallback)
		{
			_animationManager.Play(_internalAnimation);
			_internalAnimation.Play(() =>
				{
					_animationManager.Stop(_internalAnimation);
					completeCallback();
				}
				);
		}
	}
}
