﻿namespace DropGame.Animation
{
	public static class AnimationHelper
	{
		public static void Play(this IAnimation animation)
		{
			animation.Play(delegate { });
		}
	}
}
