namespace DropGame.Animation
{
	public interface IBoardCellAnimation : IAnimation
	{
		void Initialize(BoardPosition position);
	}

	public interface IStaticBoardCellAnimation : IPerpetualAnimation, IBoardCellAnimation
	{
	}
}
