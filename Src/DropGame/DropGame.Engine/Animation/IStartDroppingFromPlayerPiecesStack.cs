﻿namespace DropGame.Animation
{
	public interface IStartDroppingFromPlayerPiecesStack : IAnimation
	{
		void Initialize(int stackIndex, IPlayer player);
	}
}
