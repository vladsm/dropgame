﻿namespace DropGame.Animation
{
	public interface IPlayerPiecesStackAnimation : IPerpetualAnimation
	{
		void Initialize(int column, IPlayer player);
	}
}
