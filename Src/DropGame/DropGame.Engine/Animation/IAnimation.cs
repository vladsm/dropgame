﻿using System;

namespace DropGame.Animation
{
	public interface IAnimation
	{
		void Play([NotNull] Action completeCallback);
	}

	public interface IPerpetualAnimation : IAnimation
	{
		void Stop();
	}
}
