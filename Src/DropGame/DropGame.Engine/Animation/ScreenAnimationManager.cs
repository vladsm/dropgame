using System.Collections.Generic;

namespace DropGame.Animation
{
	public class ScreenAnimationManager
	{
		private readonly List<IAnimation> _activeAnimations = new List<IAnimation>();
		
		public void Play(IAnimation animation)
		{
			_activeAnimations.Add(animation);
		}

		public void Stop(IAnimation animation)
		{
			_activeAnimations.Remove(animation);
		}

		public IEnumerable<IAnimation> ActiveAnimations
		{
			get { return _activeAnimations; }
		}
	}
}
