﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DropGame.Animation
{
	public class AnimationQueue
	{
		private readonly Queue<IAnimation> _internalQueue = new Queue<IAnimation>();

		public event EventHandler Completed;

		public void Play(IAnimation animation)
		{
			bool playImmediately = !_internalQueue.Any();
			_internalQueue.Enqueue(animation);
			if (playImmediately)
			{
				animation.Play(PlayComplete);
			}
		}

		public bool IsEmpty
		{
			get { return !_internalQueue.Any(); }
		}

		private void PlayComplete()
		{
			_internalQueue.Dequeue();

			if (!_internalQueue.Any())
			{
				if (Completed != null) Completed(this, EventArgs.Empty);
				return;
			}
			
			IAnimation animation = _internalQueue.Peek();
			animation.Play(PlayComplete);
		}
	}
}
