﻿namespace DropGame
{
	public enum DropDirection
	{
		Up,
		Down,
		Left,
		Right
	}
}
