using System.Collections.Generic;

namespace DropGame
{
	internal interface IPlayersRotator
	{
		[NotNull]
		PlayerEngine Current { get; }
		
		[NotNull]
		PlayerEngine Next();

		[NotNull]
		IEnumerable<PlayerEngine> AllPlayers();
	}
}
