using System;
using System.Collections;
using System.Collections.Generic;

using DropGame.Animation;

namespace DropGame
{
	internal class SimplePlayerPiecesStack : IPlayerPiecesStack
	{
		private readonly IPlayer _player;
		private readonly List<IPlayerPiece> _pieces;
		private IPlayerPiecesStackAnimation _animation;

		public SimplePlayerPiecesStack(
			[NotNull] IPlayer player,
			[NotNull] IEnumerable<IPlayerPiece> pieces
			)
		{
			if (player == null) throw new ArgumentNullException("player");
			if (pieces == null) throw new ArgumentNullException("pieces");
			_player = player;
			_pieces = new List<IPlayerPiece>(pieces);
		}

		public IEnumerator<IPlayerPiece> GetEnumerator()
		{
			return _pieces.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		public IPlayerPiece Pop()
		{
			if (_pieces.Count == 0) return null;
			var piece = _pieces[0];
			_pieces.RemoveAt(0);

			if (_pieces.Count == 0)
			{
				_animation.Stop();
			}

			return piece;
		}

		public void Show(int column, SceneEngine sceneEngine)
		{
			if (_pieces.Count == 0) return;
			if (_animation != null) return;

			sceneEngine.
				AttachAnimation(animation => animation.Initialize(column, _player), out _animation).
				Play();
		}

		public void Hide(int column, SceneEngine sceneEngine)
		{
			if (_animation != null)
			{
				_animation.Stop();
				_animation = null;
			}
		}
	}
}
