using System.Collections.Generic;

namespace DropGame
{
	public interface IPlayerPiecesStack : IEnumerable<IPlayerPiece>
	{
		IPlayerPiece Pop();
		void Show(int column, SceneEngine sceneEngine);
		void Hide(int column, SceneEngine sceneEngine);
	}
}
