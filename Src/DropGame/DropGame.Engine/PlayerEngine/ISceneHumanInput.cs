using System;

namespace DropGame
{
	public interface ISceneHumanInput
	{
		/// <summary>
		/// Raised when the human player selected the pieces stack.
		/// Callback argument contains index of the stack.
		/// </summary>
		event Action<int> PlayerPiecesStackSelected;
	}
}
