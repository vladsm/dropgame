using System;
using System.Diagnostics;

namespace DropGame
{
	public class HumanPlayerEngine : PlayerEngine
	{
		private readonly ISceneHumanInput _humanInput;
		private Action<int> _playerPiecesStackSelectedHandler;
		
		public HumanPlayerEngine(
			[NotNull] IPlayerPiecesStack[] outstandingPiecesStacks,
			[NotNull] ISceneHumanInput humanInput
			)
			: base(outstandingPiecesStacks)
		{
			if (humanInput == null) throw new ArgumentNullException("humanInput");
			_humanInput = humanInput;
		}

		public override void DoAct(PlayerActionContext context)
		{
			if (context == null) throw new ArgumentNullException("context");
			
			Debug.Assert(_playerPiecesStackSelectedHandler == null, "The player is already acting.");
			
			// HACK: Actually I don't like this technique with storing lambda expression in the class state
			// but I decided to use it currently to simplify user input subscribing.
			_playerPiecesStackSelectedHandler = 
				stackIndex => DropPieceInternal(stackIndex, context);
			_humanInput.PlayerPiecesStackSelected += _playerPiecesStackSelectedHandler;
		}

		private void DropPieceInternal(int stackIndex, PlayerActionContext context)
		{
			if (!DropPiece(stackIndex, context)) return; // piece has not been dropped
			
			if (_playerPiecesStackSelectedHandler != null)
			{
				_humanInput.PlayerPiecesStackSelected -= _playerPiecesStackSelectedHandler;
				_playerPiecesStackSelectedHandler = null;
			}
		}
	}
}
