﻿using System;

using DropGame.Animation;

namespace DropGame
{
	public delegate void PlayerActionCompleteCallback();

	public class PlayerActionContext
	{
		public PlayerActionCompleteCallback CompleteCallback { get; private set; }
		
		public SceneEngine SceneEngine { get; private set; }

		public AnimationQueue Animations { get; private set; }

		public IPlayer Player { get; set; }

		public PlayerActionContext(
			[NotNull] SceneEngine sceneEngine,
			[NotNull] PlayerActionCompleteCallback completeCallback
			)
		{
			if (sceneEngine == null) throw new ArgumentNullException("sceneEngine");
			if (completeCallback == null) throw new ArgumentNullException("completeCallback");
			SceneEngine = sceneEngine;
			CompleteCallback = completeCallback;
			Animations = new AnimationQueue();
		}
	}
}
