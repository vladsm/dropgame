using System;
using System.Diagnostics;
using System.Linq;

namespace DropGame
{
	public abstract class PlayerEngine
	{
		private readonly IPlayerPiecesStack[] _outstandingPiecesStacks;

		[NotNull]
		protected IPlayerPiecesStack[] OutstandingPiecesStacks
		{
			get { return _outstandingPiecesStacks; }
		}

		protected PlayerEngine([NotNull] IPlayerPiecesStack[] outstandingPiecesStacks)
		{
			if (outstandingPiecesStacks == null) throw new ArgumentNullException("outstandingPiecesStacks");
			_outstandingPiecesStacks = outstandingPiecesStacks;
		}

		internal void Act([NotNull] PlayerActionContext context)
		{
			// TODO: Set the player into context
			//context.Player = 
			ShowOutstandingPieces(context.SceneEngine);
			DoAct(context);
		}

		public void StopActing(SceneEngine sceneEngine)
		{
			HideOutstandingPieces(sceneEngine);
		}

		private void ShowOutstandingPieces(SceneEngine sceneEngine)
		{
			for (int column = 0; column < OutstandingPiecesStacks.Length; ++column)
			{
				OutstandingPiecesStacks[column].Show(column, sceneEngine);
			}
		}

		private void HideOutstandingPieces(SceneEngine sceneEngine)
		{
			for (int column = 0; column < OutstandingPiecesStacks.Length; ++column)
			{
				OutstandingPiecesStacks[column].Hide(column, sceneEngine);
			}
		}

		public abstract void DoAct([NotNull] PlayerActionContext context);

		public bool HasOutstandingPieces
		{
			get { return _outstandingPiecesStacks.Any(stack => stack.Any()); }
		}

		protected bool DropPiece(int stackIndex, [NotNull] PlayerActionContext context)
		{
			Debug.Assert(stackIndex > 0 && stackIndex < OutstandingPiecesStacks.Length);
			
			IPlayerPiece piece = OutstandingPiecesStacks[stackIndex].Pop();
			if (piece == null) return false; // no pieces in the stack

			context.SceneEngine.StartDroppedPiece(piece, stackIndex, context);
			return true;
		}
	}
}
