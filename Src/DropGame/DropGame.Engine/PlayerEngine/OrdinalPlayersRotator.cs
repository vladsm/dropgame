using System;
using System.Collections.Generic;
using System.Linq;

namespace DropGame
{
	class OrdinalPlayersRotator : IPlayersRotator
	{
		private readonly PlayerEngine[] _players;
		private int _currentPlayerIndex;

		public OrdinalPlayersRotator([NotNull] IEnumerable<PlayerEngine> players)
		{
			if (players == null) throw new ArgumentNullException("players");
			_players = players.ToArray();
			if (_players.Length == 0) throw new ArgumentException("Players list shouldn't be empty.");
		}

		public PlayerEngine Current
		{
			get { return _players[_currentPlayerIndex]; }
		}

		public PlayerEngine Next()
		{
			_currentPlayerIndex = (_currentPlayerIndex + 1)%_players.Length;
			return _players[_currentPlayerIndex];
		}

		public IEnumerable<PlayerEngine> AllPlayers()
		{
			return _players;
		}
	}
}
