﻿using System;
using System.Linq;

using DropGame.Animation;

namespace DropGame
{
	public class SceneEngine : ScreenEngine
	{
		private readonly IPlayersRotator _playersRotator;
		private readonly DropInteractionDispatcher _dropInteractionDispatcher = new DropInteractionDispatcher();

		public BoardState Board { get; private set; }

		internal SceneEngine(
			[NotNull] BoardState board,
			[NotNull] IPlayersRotator playersRotator,
			[NotNull] GameEngine game
			) :
			base(game)
		{
			if (board == null) throw new ArgumentNullException("board");
			if (playersRotator == null) throw new ArgumentNullException("playersRotator");
			Board = board;
			_playersRotator = playersRotator;
		}

		public void Show()
		{
			ShowBoard();
		}

		public void Start()
		{
			var context = new PlayerActionContext(this, FinishOrGoToNextPlayer);
			_playersRotator.Current.Act(context);
		}


		private void ShowBoard()
		{
			Board.ForEachItems(boardItem =>
				{
					var cell = boardItem as BoardCell;
					if (cell != null) cell.Show(this);
				}
				);
		}

		private void FinishOrGoToNextPlayer()
		{
			_playersRotator.Current.StopActing(this);

			if (CanFinish())
			{
				Finish();
				return;
			}

			var context = new PlayerActionContext(this, FinishOrGoToNextPlayer);
			_playersRotator.Next().Act(context);
		}

		private bool CanFinish()
		{
			return _playersRotator.AllPlayers().All(
				playerState => !playerState.HasOutstandingPieces
				);
		}

		private void Finish()
		{
		}

		internal void StartDroppedPiece(
			IPlayerPiece piece,
			int fromStackIndex,
			PlayerActionContext actionContext
			)
		{
			var droppingContext = new DroppingContext(piece, actionContext);
			
			BoardPosition startPosition, targetPosition;
			Board.GetStartAndTargetPositionsForDroppedPiece(
				fromStackIndex,
				out startPosition,
				out targetPosition
				);
			droppingContext.FromPosition = startPosition;
			droppingContext.ToPosition = targetPosition;

			//var startDroppingAnimation = 
			//	droppingContext.SceneEngine.AttachAnimation<IStartDroppingFromPlayerPiecesStack>(
			//	a => a.Initialize(fromStackIndex, actionContext.Player)
			//	);
			
			MovePiece(droppingContext);
		}

		internal void MovePiece(DroppingContext droppingContext)
		{
			IBoardItem targetBoardItem = Board.GetItemAtPosition(droppingContext.ToPosition);
			var interaction = _dropInteractionDispatcher.InteractionFor(droppingContext, targetBoardItem);
			interaction.Interact(droppingContext, targetBoardItem);
		}
	}
}
