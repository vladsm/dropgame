﻿namespace DropGame
{
	public interface IServiceLocator
	{
		TService GetService<TService>();
		TService CreateService<TService>();
	}
}
