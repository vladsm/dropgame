﻿using System;
using System.Collections.Generic;

namespace DropGame
{
	internal class DropInteractionDispatcher
	{
		private readonly Dictionary<InteractionDispatchKey, Func<IDropInteraction>> _interactionDispatchMap =
			new Dictionary<InteractionDispatchKey, Func<IDropInteraction>>(
				InteractionDispatchKey.EqualityComparer
				);
			//{
			//	// To register specific interaction add the new item here in the following way:
			//	{
			//		new InteractionDispatchKey(typeof(SwampBoardItem), typeof(FrogPlayerPiece)),
			//		() => new FrogAtSwampBoardInteraction()
			//	}
			//};
		
		public IDropInteraction InteractionFor(
			[NotNull] DroppingContext droppingContext,
			[NotNull] IBoardItem boardItem
			)
		{
			if (droppingContext == null) throw new ArgumentNullException("droppingContext");
			if (boardItem == null) throw new ArgumentNullException("boardItem");
			
			Func<IDropInteraction> interactionCreator;
			bool interactionFound = _interactionDispatchMap.TryGetValue(
				new InteractionDispatchKey(boardItem, droppingContext.Piece),
				out interactionCreator
				);
			return interactionFound ? interactionCreator() : new TargetBasedDropInteraction();
		}

		#region class InteractionDispatchKey

		private sealed class InteractionDispatchKey
		{
			private Type BoardItemType { get; set; }
			private Type PlayerPieceType { get; set; }

			[UsedImplicitly]
			public InteractionDispatchKey(Type boardItemType, Type playerPieceType)
			{
				BoardItemType = boardItemType;
				PlayerPieceType = playerPieceType;
			}

			public InteractionDispatchKey(IBoardItem boardItem, IPlayerPiece playerPiece)
			{
				BoardItemType = boardItem.GetType();
				PlayerPieceType = playerPiece.GetType();
			}

			#region class DefaultEqualityComparer

			private static readonly IEqualityComparer<InteractionDispatchKey> _equalityComparer = new DefaultEqualityComparer();

			public static IEqualityComparer<InteractionDispatchKey> EqualityComparer
			{
				get { return _equalityComparer; }
			}

			private sealed class DefaultEqualityComparer : IEqualityComparer<InteractionDispatchKey>
			{
				public bool Equals(InteractionDispatchKey x, InteractionDispatchKey y)
				{
					if (ReferenceEquals(x, y)) return true;
					if (ReferenceEquals(x, null)) return false;
					if (ReferenceEquals(y, null)) return false;
					if (x.GetType() != y.GetType()) return false;
					return x.BoardItemType == y.BoardItemType && x.PlayerPieceType == y.PlayerPieceType;
				}

				public int GetHashCode(InteractionDispatchKey obj)
				{
					unchecked
					{
						return (obj.BoardItemType.GetHashCode()*397) ^ obj.PlayerPieceType.GetHashCode();
					}
				}
			}

			#endregion
		}

		#endregion
	}
}
