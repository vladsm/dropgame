﻿using DropGame.Animation;

namespace DropGame
{
	public class Obstacle : BoardCell<IDefaultObstacleAnimation>
	{
		public override void AcceptDroppingPiece(DroppingContext droppingContext)
		{
			// No changes at the board
			droppingContext.Stop();
		}
	}
}
