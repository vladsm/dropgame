﻿namespace DropGame
{
	public interface IBoardItem
	{
		void AcceptDroppingPiece([NotNull] DroppingContext droppingContext);
	}
}
