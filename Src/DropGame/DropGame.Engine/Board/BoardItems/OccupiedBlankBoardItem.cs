﻿namespace DropGame
{
	public sealed class OccupiedBlankBoardItem : OccupiedBoardItem<BlankBoardItem>
	{
		public OccupiedBlankBoardItem(
			[NotNull] IPlayerPiece occupant,
			[NotNull] BlankBoardItem unoccupiedBoardItem)
			: base(occupant, unoccupiedBoardItem)
		{
		}

		public override void AcceptDroppingPiece(DroppingContext droppingContext)
		{
			// No changes at the board
			droppingContext.Stop();
		}
	}
}
