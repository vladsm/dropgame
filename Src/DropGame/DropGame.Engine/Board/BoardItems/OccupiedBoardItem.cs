﻿using System;

namespace DropGame
{
	public interface IOccupiedBoardItem : IBoardItem
	{
		IPlayerPiece Occupant { get; }
		void Leave([NotNull] DroppingContext droppingContext);
	}

	public abstract class OccupiedBoardItem<TUnoccupiedBoardItem> : IOccupiedBoardItem
		where TUnoccupiedBoardItem : class, IBoardItem
	{
		private readonly TUnoccupiedBoardItem _unoccupiedBoardItem;
		public IPlayerPiece Occupant { get; private set; }

		public abstract void AcceptDroppingPiece(DroppingContext droppingContext);

		protected OccupiedBoardItem(
			[NotNull] IPlayerPiece occupant,
			[NotNull] TUnoccupiedBoardItem unoccupiedBoardItem
			)
		{
			if (occupant == null) throw new ArgumentNullException("occupant");
			if (unoccupiedBoardItem == null) throw new ArgumentNullException("unoccupiedBoardItem");
			_unoccupiedBoardItem = unoccupiedBoardItem;
			Occupant = occupant;
		}

		public virtual void Leave(DroppingContext droppingContext)
		{
			if (droppingContext == null) throw new ArgumentNullException("droppingContext");

			droppingContext.SceneEngine.Board.SetItemAtPosition(
				_unoccupiedBoardItem,
				droppingContext.FromPosition
				);
		}
	}
}
