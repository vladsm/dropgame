namespace DropGame
{
	public abstract class Border : IBoardItem
	{
		public void AcceptDroppingPiece(DroppingContext droppingContext)
		{
			// No changes at the board
			droppingContext.Stop();
		}
	}
	
	public sealed class LeftBorder : Border
	{
	}

	public sealed class RightBorder : Border
	{
	}
}
