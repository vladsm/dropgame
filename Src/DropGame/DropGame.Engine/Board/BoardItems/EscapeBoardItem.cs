﻿namespace DropGame
{
	public sealed class EscapeBoardItem : IBoardItem
	{
		public void AcceptDroppingPiece(DroppingContext droppingContext)
		{
			droppingContext.LeavePreviousBoardItem();
			droppingContext.Stop();
		}
	}
}
