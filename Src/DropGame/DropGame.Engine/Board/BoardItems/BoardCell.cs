﻿using DropGame.Animation;

namespace DropGame
{
	public abstract class BoardCell : IBoardItem
	{
		protected BoardPosition Position { get; private set; }
		public abstract void AcceptDroppingPiece(DroppingContext droppingContext);

		internal virtual void Initialize(BoardPosition position)
		{
			Position = position;
		}

		internal abstract void Show(SceneEngine sceneEngine);
	}

	public abstract class BoardCell<TDefaultAnimation> :
		BoardCell
		where TDefaultAnimation : class, IBoardCellAnimation
	{
		protected TDefaultAnimation DefaultAnimation { get; set; }

		internal override void Show(SceneEngine sceneEngine)
		{
			if (DefaultAnimation != null) return;

			TDefaultAnimation defaultAnimation;
			sceneEngine.
				AttachAnimation(animation => animation.Initialize(Position), out defaultAnimation).
				Play();
			DefaultAnimation = defaultAnimation;
		}
	}
}
