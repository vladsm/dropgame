using DropGame.Animation;

namespace DropGame
{
	public sealed class BlankBoardItem : BoardCell<IBlankCellAnimation>
	{
		public override void AcceptDroppingPiece(DroppingContext droppingContext)
		{
			droppingContext.LeavePreviousBoardItem();
			droppingContext.SceneEngine.Board.SetItemAtPosition(
				new OccupiedBlankBoardItem(droppingContext.Piece, this),
				droppingContext.ToPosition
				);
			droppingContext.MoveDown();
		}
	}
}
