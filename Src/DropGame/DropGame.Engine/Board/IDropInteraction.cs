namespace DropGame
{
	internal interface IDropInteraction
	{
		void Interact([NotNull] DroppingContext droppingContext, [NotNull] IBoardItem boardItem);
	}
}
