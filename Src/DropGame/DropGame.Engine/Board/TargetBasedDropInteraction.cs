namespace DropGame
{
	internal class TargetBasedDropInteraction : IDropInteraction
	{
		public void Interact(DroppingContext droppingContext, IBoardItem boardItem)
		{
			boardItem.AcceptDroppingPiece(droppingContext);
		}
	}
}
