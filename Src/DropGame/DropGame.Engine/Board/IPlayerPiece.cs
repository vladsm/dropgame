﻿using System;

namespace DropGame
{
	public interface IPlayerPiece
	{
		IPlayer Player { get; }
	}

	internal abstract class PlayerPiece : IPlayerPiece
	{
		public IPlayer Player { get; private set; }

		protected PlayerPiece([NotNull] IPlayer player)
		{
			if (player == null) throw new ArgumentNullException("player");
			Player = player;
		}
	}

	internal class OrdinalPlayerPiece : PlayerPiece
	{
		public OrdinalPlayerPiece([NotNull] IPlayer player) : base(player)
		{
		}
	}
}
