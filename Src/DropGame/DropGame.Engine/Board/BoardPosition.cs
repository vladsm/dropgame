﻿namespace DropGame
{
	public struct BoardPosition
	{
		public int Column;
		public int Row;

		public BoardPosition(int column, int row)
		{
			Column = column;
			Row = row;
		}

		public BoardPosition(BoardPosition position)
		{
			Column = position.Column;
			Row = position.Row;
		}

		public BoardPosition Up
		{
			get { return new BoardPosition(Column, Row - 1);}
		}

		public BoardPosition Down
		{
			get { return new BoardPosition(Column, Row + 1);}
		}

		public BoardPosition Left
		{
			get { return new BoardPosition(Column - 1, Row);}
		}

		public BoardPosition Right
		{
			get { return new BoardPosition(Column + 1, Row);}
		}
	}
}
