﻿using System;
using System.Diagnostics;

namespace DropGame
{
	public class BoardState
	{
		private readonly IBoardItem[,] _boardItems;

		public BoardState(BoardCell[,] boardItems)
		{
			_boardItems = new IBoardItem[boardItems.GetUpperBound(0) + 1, boardItems.GetUpperBound(1) + 1];
			for (int column = 0; column < ColumnsCount; ++column)
				for (int row = 0; row < RowsCount; ++row)
				{
					boardItems[column, row].Initialize(new BoardPosition(column, row));
					_boardItems[column, row] = boardItems[column, row];
				}
		}

		public int ColumnsCount
		{
			get { return _boardItems.GetUpperBound(0) + 1; }
		}

		public int RowsCount
		{
			get { return _boardItems.GetUpperBound(1) + 1; }
		}


		internal void ForEachItems([NotNull] Action<IBoardItem> action)
		{
			ForEachItems((boardItem, column, row) => action(boardItem));
		}

		internal void ForEachItems([NotNull] Action<IBoardItem, int, int> action)
		{
			if (action == null) throw new ArgumentNullException("action");
			for (int column = 0; column < ColumnsCount; ++column)
				for (int row = 0; row < RowsCount; ++row)
				{
					action(_boardItems[column, row], column, row);
				}
		}

		internal void GetStartAndTargetPositionsForDroppedPiece(
			int playerStackIndex,
			out BoardPosition startPosition,
			out BoardPosition targetPosition
			)
		{
			int column = playerStackIndex;
			if (playerStackIndex < 0) column = 0;
			int columnsCount = ColumnsCount;
			if (playerStackIndex >= columnsCount) column = columnsCount - 1;
			
			startPosition = new BoardPosition(column, -1);
			targetPosition = new BoardPosition(column, 0);
		}

		[NotNull]
		internal IBoardItem GetItemAtPosition(BoardPosition position)
		{
			if (position.Row < 0) return new OuterSpaceBoardItem();
			if (position.Row >= RowsCount) return new EscapeBoardItem();
			if (position.Column < 0) return new LeftBorder();
			if (position.Column >= ColumnsCount) return new RightBorder();
			return _boardItems[position.Column, position.Row];
		}

		[NotNull]
		internal IBoardItem SetItemAtPosition([NotNull] IBoardItem boardItem, BoardPosition position)
		{
			if (boardItem == null) throw new ArgumentNullException("boardItem");

			Debug.Assert(IsInsideBoard(position));

			var previousBoardItem = _boardItems[position.Column, position.Row];
			_boardItems[position.Column, position.Row] = boardItem;
			return previousBoardItem;
		}

		private bool IsInsideBoard(BoardPosition position)
		{
			return position.Column >= 0 && position.Column < ColumnsCount &&
				position.Row >= 0 && position.Row < RowsCount;
		}
	}
}
