﻿using System;

namespace DropGame
{
	internal static class DroppingHelper
	{
		public static void LeavePreviousBoardItem(this DroppingContext droppingContext)
		{
			var previousBoardItem = droppingContext.SceneEngine.Board.GetItemAtPosition(
				droppingContext.FromPosition
				) as IOccupiedBoardItem;
			if (previousBoardItem != null) previousBoardItem.Leave(droppingContext);
		}

		public static void MoveToAdjacent(this DroppingContext droppingContext, DropDirection direction)
		{
			BoardPosition fromPosition = droppingContext.ToPosition;
			BoardPosition toPosition;

			switch (direction)
			{
				case DropDirection.Up:
					toPosition = fromPosition.Up;
					break;
				case DropDirection.Down:
					toPosition = fromPosition.Down;
					break;
				case DropDirection.Left:
					toPosition = fromPosition.Left;
					break;
				case DropDirection.Right:
					toPosition = fromPosition.Right;
					break;
				default:
					throw new ArgumentOutOfRangeException("direction");
			}
			
			droppingContext = new DroppingContext(droppingContext, fromPosition, toPosition);
			droppingContext.SceneEngine.MovePiece(droppingContext);
		}

		public static void MoveUp(this DroppingContext droppingContext)
		{
			droppingContext.MoveToAdjacent(DropDirection.Up);
		}
		
		public static void MoveDown(this DroppingContext droppingContext)
		{
			droppingContext.MoveToAdjacent(DropDirection.Down);
		}
		
		public static void MoveLeft(this DroppingContext droppingContext)
		{
			droppingContext.MoveToAdjacent(DropDirection.Left);
		}
		
		public static void MoveRight(this DroppingContext droppingContext)
		{
			droppingContext.MoveToAdjacent(DropDirection.Right);
		}

		public static void Stop(this DroppingContext droppingContext)
		{
			if (droppingContext.Animations.IsEmpty)
			{
				droppingContext.PlayerActionContext.CompleteCallback();
			}
			else
			{
				// HACK: Subscribing to event without unsubscribing. It needs to find out how 
				// critical it is and the way to fix it.
				droppingContext.Animations.Completed += delegate
				{
					droppingContext.PlayerActionContext.CompleteCallback();
				};
			}
		}
	}
}
