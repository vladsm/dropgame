using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using DropGame.Animation;

using Microsoft.Xna.Framework.Input.Touch;

using PortableIoC;

using Windows.Devices.Input;

namespace DropGame.WindowsStore
{
	internal sealed class DropGameImplementation : Game
	{
		private readonly GraphicsDeviceManager _graphics;
		private SpriteBatch _spriteBatch;
		private SceneEngine _sceneEngine;
		private GameEngine _gameEngine;
		private SceneHumanInput _sceneHumanInput;
		private List<SceneHumanInputDriver> _sceneHumanInputDrivers;

		public IGameLayoutManager Layout { get; private set; }

		public DropGameImplementation()
		{
			_graphics = new GraphicsDeviceManager(this);
			Content.RootDirectory = "Content";

#if DEBUG
			Components.Add(new FrameRateCounter(this));
			_graphics.SynchronizeWithVerticalRetrace = false;
			IsFixedTimeStep = false;
#endif
		}

		/// <summary>
		/// Allows the game to perform any initialization it needs to before starting to run.
		/// This is where it can query for any required services and load any non-graphic
		/// related content.  Calling base.Initialize will enumerate through any components
		/// and initialize them as well.
		/// </summary>
		protected override void Initialize()
		{
			UpdateResolution();
			Window.ClientSizeChanged += delegate { UpdateResolution(); };
			
			_spriteBatch = new SpriteBatch(GraphicsDevice);

			InitializeHumanInput();
			InitializeGameEngine();
			CreateAndRunScene();

			base.Initialize();
		}

		/// <summary>
		/// LoadContent will be called once per game and is the place to load
		/// all of your content.
		/// </summary>
		protected override void LoadContent()
		{
			// TODO: use this.Content to load your game content here
		}

		/// <summary>
		/// UnloadContent will be called once per game and is the place to unload
		/// all content.
		/// </summary>
		protected override void UnloadContent()
		{
			// TODO: Unload any non ContentManager content here
		}

		/// <summary>
		/// Allows the game to run logic such as updating the world,
		/// checking for collisions, gathering input, and playing audio.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Update(GameTime gameTime)
		{
			foreach (var sceneHumanInputDriver in _sceneHumanInputDrivers)
			{
				sceneHumanInputDriver.ProcessInput();
			}
			
			//bool canStop = false;
			//while (TouchPanel.IsGestureAvailable)
			//{
			//	var gesture = TouchPanel.ReadGesture();
			//	if (gesture.GestureType == GestureType.Tap)
			//	{
			//		canStop = true;
			//	}
			//}

			//if (canStop)
			//{
			//	var activeAnimations = new List<IAnimation>(_sceneEngine.ActiveAnimations);
			//	foreach (var animation in activeAnimations)
			//	{
			//		var obstacleAnimation = animation as IDefaultObstacleAnimation;
			//		if (obstacleAnimation != null) obstacleAnimation.Stop();
			//	}
			//}
			
			base.Update(gameTime);
		}

		/// <summary>
		/// This is called when the game should draw itself.
		/// </summary>
		/// <param name="gameTime">Provides a snapshot of timing values.</param>
		protected override void Draw(GameTime gameTime)
		{
			GraphicsDevice.Clear(Color.CornflowerBlue);

			_spriteBatch.Begin();

			foreach (var animation in _sceneEngine.ActiveAnimations)
			{
				var drawable = animation as IPlayableAnimation;
				if (drawable != null) drawable.Draw(_spriteBatch, gameTime);
			}

			_spriteBatch.End();

			base.Draw(gameTime);
		}


		private void UpdateResolution()
		{
			Layout = CreateGameLayout();
			
			Content.Unload();
			Content = Layout.Content;
			Content.RootDirectory = "Content";
		}

		private const float ScaleFactorTollerance = 0.001f;

		private IGameLayoutManager CreateGameLayout()
		{
			Viewport viewport = GraphicsDevice.Viewport;
			// HACK: For some reason sometimes viewport has 1px less sizes, than device stated.
			if (viewport.Width % 2 != 0) viewport.Width++;
			if (viewport.Height % 2 != 0) viewport.Height++;

			float viewportAspectRatio = (float)viewport.Height/viewport.Width;
			// investigation shows that the 0.73 is pretty optimal value for the layout
			// type switching threshold
			bool isHorisontalLayout = viewportAspectRatio < 0.73f;

			// The basic resolution for all sizing calculations is 1366 x 768.

			float scaleFactor = isHorisontalLayout ?
				// For the horizontal layouts the basic resolution is 1366 x 768.
				// So we are scaling up from it.
				viewport.Width/1366f :
				// For the vertical layouts the basic resolution is 1024 x 768. It has scale factor 0.85.
				// So we are scaling up from it.
				viewport.Width/1024f*0.85f;

			// Native scale factors are the special because the non-scaled graphic assets
			// will be provided for them. So it is good to exclude graphic scaling when the
			// scale factor differs a little from native.
			var nativeGraphicsScaleFactors = new float?[] { 1f, 1.406f };
			float scaleFactorClosure = scaleFactor;
			float? sufficientNativeScaleFactor = nativeGraphicsScaleFactors.FirstOrDefault(
				nsf => nsf.HasValue && Math.Abs(nsf.Value - scaleFactorClosure) < ScaleFactorTollerance
				);
			if (sufficientNativeScaleFactor.HasValue) scaleFactor = sufficientNativeScaleFactor.Value;

			IScalingTransformer scalingTransformer = Math.Abs(scaleFactor - 1f) < ScaleFactorTollerance ?
				(IScalingTransformer)new InvariantScalingTransformer() :
				new ScalingTransformer(scaleFactor);

			if (isHorisontalLayout)
			{
				DropGameContentManagerBase contentManager = GetContentManagerByScaleFactor(
					scaleFactor,
					nativeGraphicsScaleFactors.Cast<float>().ToArray(),
					new Func<DropGameContentManagerBase>[]
					{
						() => new HorizontalDefaultResolutionContentManager(Services),
						() => new HorizontalHighResolutionContentManager(Services)
					}
					);
				return new HorizontalLargeScreenGameLayoutManager(viewport, scalingTransformer, contentManager);
			}
			else
			{
				DropGameContentManagerBase contentManager = GetContentManagerByScaleFactor(
					scaleFactor,
					nativeGraphicsScaleFactors.Cast<float>().ToArray(),
					new Func<DropGameContentManagerBase>[]
					{
						() => new VerticalDefaultResolutionContentManager(Services),
						() => new VerticalHighResolutionContentManager(Services)
					}
					);
				return new VerticalLargeScreenGameLayoutManager(viewport, scalingTransformer, contentManager);
			}
		}

		private DropGameContentManagerBase GetContentManagerByScaleFactor(
			float scaleFactor,
			float[] scaleFactorPoints,
			Func<DropGameContentManagerBase>[] contentManagerCreatorsForPoints
			)
		{
			for (int i = 0; i < scaleFactorPoints.Length; ++i)
			{
				if (scaleFactor <= scaleFactorPoints[i] + ScaleFactorTollerance)
				{
					return contentManagerCreatorsForPoints[i]();
				}
			}
			return contentManagerCreatorsForPoints.Last()();
		}

		private void InitializeGameEngine()
		{
			var iocContainer = new PortableIoc();
			iocContainer.Register<IDefaultObstacleAnimation>(ioc => new DefaultObstacleAnimation(this));
			iocContainer.Register<IBlankCellAnimation>(ioc => new BlankCellAnimation(this));
			iocContainer.Register<IPlayerPiecesStackAnimation>(ioc => new PlayerPieceStackAnimation(this));

			_gameEngine = new GameEngine(new GameServiceLocator(iocContainer));
		}

		private void InitializeHumanInput()
		{
			_sceneHumanInput = new SceneHumanInput();
			_sceneHumanInputDrivers = new List<SceneHumanInputDriver>();
			foreach (var pointerDevice in PointerDevice.GetPointerDevices())
			{
				SceneHumanInputDriver newSceneInputDriver = null;
				switch (pointerDevice.PointerDeviceType)
				{
					case PointerDeviceType.Mouse:
					case PointerDeviceType.Pen:
						newSceneInputDriver = new SceneHumanInputMouseDriver(_sceneHumanInput, this);
						break;
					case PointerDeviceType.Touch:
						newSceneInputDriver = new SceneHumanInputTouchDriver(_sceneHumanInput, this);
						break;
				}
				// TODO: Provide nice processing for this error
				Debug.Assert(newSceneInputDriver == null, "No supported human input types found");

				if (_sceneHumanInputDrivers.Contains(newSceneInputDriver)) continue;
				_sceneHumanInputDrivers.Add(newSceneInputDriver);
			}
		}

		#region Temporal code

		private void CreateAndRunScene()
		{
			const int boardSizeX = 20;
			const int boardSizeY = 18;
			var boardItems = new BoardCell[boardSizeX,boardSizeY];
			for (int column = 0; column < boardSizeX; ++column)
				for (int row = 0; row < boardSizeY; ++row)
				{
					if ((column > 4 && column < 13 || column == 17) && (row > 11 && row < 16))
						boardItems[column, row] = new BlankBoardItem();
					else
						boardItems[column, row] = new Obstacle();
				}

			_sceneEngine = _gameEngine.CreateSceneScreen(
				new IPlayer[] { new GreenPlayer(), new RedPlayer() },
				new BoardState(boardItems),
				_sceneHumanInput
				);

			Layout.Scene.Board.SetBoardSize(boardSizeX, boardSizeY);

			_sceneEngine.Show();
			_sceneEngine.Start();
		}

		#endregion
	}
}
