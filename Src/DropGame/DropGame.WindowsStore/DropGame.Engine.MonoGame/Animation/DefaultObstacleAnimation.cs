﻿using DropGame.WindowsStore;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DropGame.Animation
{
	sealed class DefaultObstacleAnimation : BoardCellAnimationBase, IDefaultObstacleAnimation
	{
		private readonly ReloadableTexture2D _texture;

		public DefaultObstacleAnimation(DropGameImplementation game) : base(game)
		{
			_texture = Game.GetReloadableTexture2D("textures/obstacle");
		}

		protected override void DrawActiveCell(SpriteBatch spriteBatch, GameTime gameTime, Vector2 cellPosition, Vector2 cellSize)
		{
			spriteBatch.Draw(_texture, cellPosition, cellSize);
		}
	}
}
