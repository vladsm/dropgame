using DropGame.WindowsStore;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DropGame.Animation
{
	abstract class BoardCellAnimationBase : AnimationBase
	{
		protected BoardCellAnimationBase(DropGameImplementation game) : base(game)
		{
		}

		protected BoardPosition Position { get; set; }
		
		public IBoardLayout BoardLayout
		{
			get { return Game.Layout.Scene.Board; }
		}

		public void Initialize(BoardPosition position)
		{
			Position = position;
		}

		public void Stop()
		{
			if (CompleteCallback == null) return;
			CompleteCallback();
		}

		protected override void DrawActive(SpriteBatch spriteBatch, GameTime gameTime)
		{
			var size = BoardLayout.CellSize;
			var position = BoardLayout.CellsPosition + new Vector2(Position.Column * size.X, Position.Row * size.Y);
			DrawActiveCell(spriteBatch, gameTime, position, size);
		}

		protected abstract void DrawActiveCell(SpriteBatch spriteBatch, GameTime gameTime, Vector2 cellPosition, Vector2 cellSize);
	}
}
