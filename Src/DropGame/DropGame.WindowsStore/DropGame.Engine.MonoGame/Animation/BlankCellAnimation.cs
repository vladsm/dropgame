﻿using DropGame.WindowsStore;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DropGame.Animation
{
	class BlankCellAnimation : BoardCellAnimationBase, IBlankCellAnimation
	{
		private readonly ReloadableTexture2D _texture;

		public BlankCellAnimation(DropGameImplementation game) : base(game)
		{
			_texture = Game.GetReloadableTexture2D("textures/star");
		}

		protected override void DrawActiveCell(SpriteBatch spriteBatch, GameTime gameTime, Vector2 cellPosition, Vector2 cellSize)
		{
			spriteBatch.Draw(_texture, cellPosition, cellSize);
		}
	}
}
