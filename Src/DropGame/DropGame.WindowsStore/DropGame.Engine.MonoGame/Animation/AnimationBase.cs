using System;

using DropGame.WindowsStore;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DropGame.Animation
{
	abstract class AnimationBase : IPlayableAnimation
	{
		protected bool IsActive { get; set; }
		protected Action CompleteCallback { get; set; }

		protected DropGameImplementation Game { get; private set; }

		protected AnimationBase(DropGameImplementation game)
		{
			Game = game;
		}

		public void Play(Action completeCallback)
		{
			if (CompleteCallback != null) return;
			CompleteCallback = completeCallback;
			IsActive = true;
		}

		public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime)
		{
			if (!IsActive) return;
			DrawActive(spriteBatch, gameTime);
		}

		protected abstract void DrawActive(SpriteBatch spriteBatch, GameTime gameTime);
	}
}
