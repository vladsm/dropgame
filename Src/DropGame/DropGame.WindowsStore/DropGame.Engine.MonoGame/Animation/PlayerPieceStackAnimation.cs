﻿using System.Diagnostics;

using DropGame.WindowsStore;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DropGame.Animation
{
	class PlayerPieceStackAnimation : AnimationBase, IPlayerPiecesStackAnimation
	{
		private int _column;
		private IPlayerAssetsProvider _playerAssets;
		private ReloadableTexture2D _texture;

		public PlayerPieceStackAnimation(DropGameImplementation game) : base(game)
		{
		}

		public void Initialize(int column, IPlayer player)
		{
			Debug.Assert(player is IPlayerAssetsProvider);

			_playerAssets = (IPlayerAssetsProvider)player;
			_column = column;
			_texture = Game.GetReloadableTexture2D(_playerAssets.OutstandingPiecesStackTexture);
		}

		public void Stop()
		{
			if (CompleteCallback == null) return;
			CompleteCallback();
		}

		protected override void DrawActive(SpriteBatch spriteBatch, GameTime gameTime)
		{
			IBoardLayout boardLayout = Game.Layout.Scene.Board;
			var size = boardLayout.OutstandingPiecesStackSize;
			var position = boardLayout.OutstandingPiecesStacksPosition + new Vector2(_column * size.X, 0);
			spriteBatch.Draw(_texture, position, size);
		}
	}
}
