using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DropGame.Animation
{
	internal interface IPlayableAnimation
	{
		void Draw(SpriteBatch spriteBatch, GameTime gameTime);
	}
}
