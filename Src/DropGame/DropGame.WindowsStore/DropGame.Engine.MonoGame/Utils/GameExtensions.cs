﻿using Microsoft.Xna.Framework;

namespace DropGame
{
	static class GameExtensions
	{
		public static ReloadableResource<TResource> GetReloadableResource<TResource>(this Game game, string name)
		{
			return new ReloadableResource<TResource>(game, name);
		}

		public static ReloadableTexture2D GetReloadableTexture2D(this Game game, string name)
		{
			return new ReloadableTexture2D(game, name);
		}
	}
}
