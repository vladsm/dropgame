﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DropGame
{
	public static partial class GraphicPrimitivesHelper
	{
		public static void Draw(
			this SpriteBatch spriteBatch,
			Texture2D texture,
			Vector2 position,
			Rectangle? sourceRectangle,
			Color color,
			float rotation,
			Vector2 origin,
			Vector2 destinationSize,
			SpriteEffects effect,
			float depth
			)
		{
			var originalSize = sourceRectangle.HasValue ?
				new Vector2(sourceRectangle.Value.X, sourceRectangle.Value.Y) :
				new Vector2(texture.Width, texture.Height);
			var scale = destinationSize.ComponentWiseDivide(originalSize);
			spriteBatch.Draw(texture, position, sourceRectangle, color, rotation, origin, scale, effect, depth);
		}

		public static void Draw(
			this SpriteBatch spriteBatch,
			Texture2D texture,
			Vector2 position,
			Color color,
			float rotation,
			Vector2 origin,
			Vector2 destinationSize,
			SpriteEffects effect,
			float depth
			)
		{
			Draw(spriteBatch, texture, position, null, color, rotation, origin, destinationSize, effect, depth);
		}

		public static void Draw(
			this SpriteBatch spriteBatch,
			Texture2D texture,
			Vector2 position,
			Color color,
			Vector2 destinationSize,
			SpriteEffects effect,
			float depth
			)
		{
			Draw(spriteBatch, texture, position, null, color, 0, Vector2.Zero, destinationSize, effect, depth);
		}

		public static void Draw(
			this SpriteBatch spriteBatch,
			Texture2D texture,
			Vector2 position,
			Vector2 destinationSize
			)
		{
			Draw(spriteBatch, texture, position, null, Color.White, 0, Vector2.Zero, destinationSize, SpriteEffects.None, 0);
		}

		public static void Draw(
			this SpriteBatch spriteBatch,
			Texture2D texture,
			Vector2 position,
			Rectangle sourceRectangle,
			Color color,
			Vector2 destinationSize,
			SpriteEffects effect,
			float depth
			)
		{
			Draw(spriteBatch, texture, position, sourceRectangle, color, 0, Vector2.Zero, destinationSize, effect, depth);
		}

		public static void Draw(
			this SpriteBatch spriteBatch,
			Texture2D texture,
			Vector2 position,
			Rectangle sourceRectangle,
			Vector2 destinationSize
			)
		{
			Draw(spriteBatch, texture, position, sourceRectangle, Color.White, 0, Vector2.Zero, destinationSize, SpriteEffects.None, 0);
		}
	}
}
