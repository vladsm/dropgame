﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace DropGame
{
	class ReloadableResource<TResource>
	{
		private readonly Game _game;
		private readonly string _name;

		public ReloadableResource([NotNull] Game game, [NotNull] string name)
		{
			if (game == null) throw new ArgumentNullException("game");
			if (name == null) throw new ArgumentNullException("name");
			_game = game;
			_name = name;
		}

		public static implicit operator TResource(ReloadableResource<TResource> obj)
		{
			return obj.GetResource();
		}

		protected virtual TResource GetResource()
		{
			return _game.Content.Load<TResource>(_name);
		}
	}


	class ReloadableTexture2D : ReloadableResource<Texture2D>
	{
		public ReloadableTexture2D([NotNull] Game game, [NotNull] string name) : base(game, name)
		{
		}
	}
}
