﻿using Microsoft.Xna.Framework;

namespace DropGame
{
	public static partial class GraphicPrimitivesHelper
	{
		public static Vector2 ComponentWiseMultiply(this Vector2 vector1, Vector2 vector2)
		{
			return new Vector2(vector1.X*vector2.X, vector1.Y*vector2.Y);
		}

		public static Vector2 ComponentWiseDivide(this Vector2 vector1, Vector2 vector2)
		{
			return new Vector2(vector1.X/vector2.X, vector1.Y/vector2.Y);
		}
	}
}
