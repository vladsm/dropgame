﻿using System;

using PortableIoC;

namespace DropGame
{
	internal class GameServiceLocator : IServiceLocator
	{
		private readonly IPortableIoC _container;

		public GameServiceLocator([NotNull] IPortableIoC container)
		{
			if (container == null) throw new ArgumentNullException("container");
			_container = container;
		}

		public TService GetService<TService>()
		{
			return _container.Resolve<TService>();
		}

		public TService CreateService<TService>()
		{
			return _container.Resolve<TService>(true);
		}
	}
}
