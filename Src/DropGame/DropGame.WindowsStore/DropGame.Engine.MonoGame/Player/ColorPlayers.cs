namespace DropGame
{
	class GreenPlayer : Player
	{
		public override string OutstandingPiecesStackTexture
		{
			get { return "textures/greenStack"; }
		}
	}

	class RedPlayer : Player
	{
		public override string OutstandingPiecesStackTexture
		{
			get { return "textures/redStack"; }
		}
	}
}
