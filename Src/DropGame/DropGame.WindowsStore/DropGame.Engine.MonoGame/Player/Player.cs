﻿namespace DropGame
{
	abstract class Player : IPlayer, IPlayerAssetsProvider
	{
		public abstract string OutstandingPiecesStackTexture { get; }
	}
}
