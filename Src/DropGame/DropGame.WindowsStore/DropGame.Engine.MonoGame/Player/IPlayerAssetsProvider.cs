namespace DropGame
{
	public interface IPlayerAssetsProvider
	{
		string OutstandingPiecesStackTexture { get; }
	}
}
