using DropGame.WindowsStore;

namespace DropGame
{
	abstract class SceneHumanInputDriver
	{
		protected DropGameImplementation Game { get; private set; }

		protected SceneHumanInputDriver(SceneHumanInput sceneHumanInput, DropGameImplementation game)
		{
			SceneHumanInput = sceneHumanInput;
			Game = game;
		}

		protected SceneHumanInput SceneHumanInput { get; private set; }

		public abstract void ProcessInput();
	}
}
