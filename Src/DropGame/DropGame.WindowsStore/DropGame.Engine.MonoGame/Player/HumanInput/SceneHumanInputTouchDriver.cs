﻿using DropGame.WindowsStore;

using Microsoft.Xna.Framework.Input.Touch;

namespace DropGame
{
	sealed class SceneHumanInputTouchDriver : SceneHumanInputDriver
	{
		public SceneHumanInputTouchDriver(SceneHumanInput sceneHumanInput, DropGameImplementation game) :
			base(sceneHumanInput, game)
		{
			TouchPanel.EnabledGestures = GestureType.Tap;
		}

		public override void ProcessInput()
		{
			while (TouchPanel.IsGestureAvailable)
			{
				var gesture = TouchPanel.ReadGesture();
				if (gesture.GestureType == GestureType.Tap)
				{
					ProcessTap(gesture);
				}
			}
		}

		private void ProcessTap(GestureSample gesture)
		{
			int? column = Game.Layout.Scene.Board.GetPlayerPiecesStackAtPosition(gesture.Position);
			if (column.HasValue)
			{
				SceneHumanInput.SelectPlayerPiecesStack(column.Value);
			}
		}
	}
}
