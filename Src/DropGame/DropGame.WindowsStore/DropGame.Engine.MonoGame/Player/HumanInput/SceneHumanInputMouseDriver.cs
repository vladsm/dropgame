using DropGame.WindowsStore;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace DropGame
{
	class SceneHumanInputMouseDriver : SceneHumanInputDriver
	{
		private MouseState _lastMouseState;

		public SceneHumanInputMouseDriver(SceneHumanInput sceneHumanInput, DropGameImplementation game) :
			base(sceneHumanInput, game)
		{
		}

		public override void ProcessInput()
		{
			MouseState mouseState = Mouse.GetState();
			if (_lastMouseState.LeftButton == ButtonState.Released && mouseState.LeftButton == ButtonState.Pressed)
			{
				ProcessClick(mouseState);
			}
			_lastMouseState = mouseState;
		}

		private void ProcessClick(MouseState mouseState)
		{
			int? column = Game.Layout.Scene.Board.GetPlayerPiecesStackAtPosition(
				new Vector2(mouseState.X, mouseState.Y)
				);
			if (column.HasValue)
			{
				SceneHumanInput.SelectPlayerPiecesStack(column.Value);
			}
		}
	}
}
