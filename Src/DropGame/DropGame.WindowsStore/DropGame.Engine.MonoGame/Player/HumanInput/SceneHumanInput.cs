﻿using System;

namespace DropGame
{
	class SceneHumanInput : ISceneHumanInput
	{
		public event Action<int> PlayerPiecesStackSelected;

		public void SelectPlayerPiecesStack(int column)
		{
			if (PlayerPiecesStackSelected != null) PlayerPiecesStackSelected(column);
		}
	}
}
