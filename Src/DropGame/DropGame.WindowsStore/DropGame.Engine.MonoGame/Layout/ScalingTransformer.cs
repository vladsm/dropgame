using Microsoft.Xna.Framework;

namespace DropGame
{
	internal interface IScalingTransformer
	{
		Vector2 Transform(Vector2 vector);
	}

	internal class InvariantScalingTransformer : IScalingTransformer
	{
		public Vector2 Transform(Vector2 vector)
		{
			return vector;
		}
	}

	internal class ScalingTransformer : IScalingTransformer
	{
		private readonly float _scaleFactor;

		public ScalingTransformer(float scaleFactor)
		{
			_scaleFactor = scaleFactor;
		}

		public Vector2 Transform(Vector2 vector)
		{
			return vector*_scaleFactor;
		}
	}
}
