using Microsoft.Xna.Framework.Graphics;

namespace DropGame
{
	internal sealed class VerticalLargeScreenGameLayoutManager : IGameLayoutManager
	{
		private readonly DropGameContentManagerBase _contentManager;
		private readonly SceneLargeScreenVerticalLayout _sceneLayout;

		public VerticalLargeScreenGameLayoutManager(
			Viewport viewport,
			IScalingTransformer scalingTransformer,
			DropGameContentManagerBase contentManager
			)
		{
			_contentManager = contentManager;
			_sceneLayout = new SceneLargeScreenVerticalLayout(viewport, scalingTransformer);
		}

		public DropGameContentManagerBase Content
		{
			get { return _contentManager; }
		}

		public ISceneScreenLayout Scene
		{
			get { return _sceneLayout; }
		}
	}

	internal sealed class HorizontalLargeScreenGameLayoutManager : IGameLayoutManager
	{
		private readonly DropGameContentManagerBase _contentManager;
		private readonly SceneLargeScreenHorizontalLayout _sceneLayout;

		public HorizontalLargeScreenGameLayoutManager(
			Viewport viewport,
			IScalingTransformer scalingTransformer,
			DropGameContentManagerBase contentManager
			)
		{
			_contentManager = contentManager;
			_sceneLayout = new SceneLargeScreenHorizontalLayout(viewport, scalingTransformer);
		}

		public DropGameContentManagerBase Content
		{
			get { return _contentManager; }
		}

		public ISceneScreenLayout Scene
		{
			get { return _sceneLayout; }
		}
	}
}
