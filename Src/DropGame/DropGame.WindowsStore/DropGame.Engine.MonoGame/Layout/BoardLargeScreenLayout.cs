using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DropGame
{
	sealed class BoardLargeScreenLayout : ScreenLayoutBase, IBoardLayout
	{
		private readonly Viewport _viewport;
		private int _columns;
		private int _rows;

		public BoardLargeScreenLayout(
			Viewport viewport,
			IScalingTransformer scaling
			)
			: base(scaling)
		{
			_viewport = viewport;

			OutstandingPiecesStacksPosition = Adopt(10, 35);
			OutstandingPiecesStackSize = Adopt(55, 60);
			CellsPosition = Adopt(10, 95);
			CellSize = Adopt(55, 35);
		}

		public Vector2 OutstandingPiecesStacksPosition { get; private set; }
		public Vector2 OutstandingPiecesStackSize { get; private set; }
		public Vector2 CellsPosition { get; private set; }
		public Vector2 CellSize { get; private set; }

		public void SetBoardSize(int columns, int rows)
		{
			_rows = rows;
			_columns = columns;
		}

		public int? GetPlayerPiecesStackAtPosition(Vector2 position)
		{
			if (position.Y < OutstandingPiecesStacksPosition.Y || 
					position.Y > OutstandingPiecesStacksPosition.Y + OutstandingPiecesStackSize.Y)
			{
				return null;
			}

			float column = (position.X - OutstandingPiecesStacksPosition.X)/OutstandingPiecesStackSize.X;
			if (column < 0 || column >= _columns) return null;
			return (int)Math.Floor(column);
		}

	}
}
