using Microsoft.Xna.Framework.Graphics;

namespace DropGame
{
	internal abstract class SceneLargeScreenLayout : ScreenLayoutBase, ISceneScreenLayout
	{
		protected Viewport Viewport { get; private set; }

		public IBoardLayout Board { get; private set; }

		protected SceneLargeScreenLayout(
			Viewport viewport,
			IScalingTransformer scaling
			)
			: base(scaling)
		{
			Viewport = viewport;
			Board = new BoardLargeScreenLayout(viewport, scaling);
		}
	}

	internal sealed class SceneLargeScreenHorizontalLayout : SceneLargeScreenLayout
	{
		public SceneLargeScreenHorizontalLayout(
			Viewport viewport,
			IScalingTransformer scaling
			)
			: base(viewport, scaling)
		{
		}
	}

	internal sealed class SceneLargeScreenVerticalLayout : SceneLargeScreenLayout
	{
		public SceneLargeScreenVerticalLayout(
			Viewport viewport,
			IScalingTransformer scaling
			)
			: base(viewport, scaling)
		{
		}
	}
}
