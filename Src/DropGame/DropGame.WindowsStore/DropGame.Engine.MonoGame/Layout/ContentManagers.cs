﻿using System;
using System.Collections.Generic;

namespace DropGame
{
	internal sealed class HorizontalDefaultResolutionContentManager : DropGameContentManagerBase
	{
		public HorizontalDefaultResolutionContentManager(IServiceProvider serviceProvider)
			: base(
				serviceProvider,
				new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
				{
					{"fonts/fps", "fonts/fps"},
					{"textures/greenstack", "textures/greenstack"},
					{"textures/obstacle", "textures/obstacle"},
					{"textures/redstack", "textures/redstack"},
					{"textures/star", "textures/star"}
						
				}
				)
		{
		}
	}

	internal sealed class HorizontalHighResolutionContentManager : DropGameContentManagerBase
	{
		public HorizontalHighResolutionContentManager(IServiceProvider serviceProvider)
			: base(
				serviceProvider,
				new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
				{
					{"fonts/fps", "fonts/fps"},
					{"textures/greenstack", "textures/greenstack"},
					{"textures/obstacle", "textures/obstacle-high"},
					{"textures/redstack", "textures/redstack"},
					{"textures/star", "textures/star"}
						
				}
				)
		{
		}
	}

	internal sealed class VerticalDefaultResolutionContentManager : DropGameContentManagerBase
	{
		public VerticalDefaultResolutionContentManager(IServiceProvider serviceProvider)
			: base(
				serviceProvider,
				new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
				{
					{"fonts/fps", "fonts/fps"},
					{"textures/greenstack", "textures/greenstack"},
					{"textures/obstacle", "textures/obstacle-v"},
					{"textures/redstack", "textures/redstack"},
					{"textures/star", "textures/star"}
						
				}
				)
		{
		}
	}

	internal sealed class VerticalHighResolutionContentManager : DropGameContentManagerBase
	{
		public VerticalHighResolutionContentManager(IServiceProvider serviceProvider)
			: base(
				serviceProvider,
				new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
				{
					{"fonts/fps", "fonts/fps"},
					{"textures/greenstack", "textures/greenstack"},
					{"textures/obstacle", "textures/obstacle-v"},
					{"textures/redstack", "textures/redstack"},
					{"textures/star", "textures/star"}
						
				}
				)
		{
		}
	}

}
