﻿using Microsoft.Xna.Framework;

namespace DropGame
{
	internal interface IGameLayoutManager
	{
		DropGameContentManagerBase Content { get; }
		ISceneScreenLayout Scene { get; }
	}

	internal interface ISceneScreenLayout
	{
		IBoardLayout Board { get; }
	}

	internal interface IBoardLayout
	{
		Vector2 OutstandingPiecesStacksPosition { get; }
		Vector2 OutstandingPiecesStackSize { get; }
		Vector2 CellsPosition { get; }
		Vector2 CellSize { get; }
		void SetBoardSize(int columns, int rows);
		int? GetPlayerPiecesStackAtPosition(Vector2 position);
	}
}
