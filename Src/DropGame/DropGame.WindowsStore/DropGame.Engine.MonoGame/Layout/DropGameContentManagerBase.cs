﻿using System;
using System.Collections.Generic;

using Microsoft.Xna.Framework.Content;

namespace DropGame
{
	internal abstract class DropGameContentManagerBase : ContentManager
	{
		private readonly IDictionary<string, string> _assetsMap;

		protected DropGameContentManagerBase(
			IServiceProvider serviceProvider,
			IDictionary<string, string> assetsMap
			)
			: base(serviceProvider)
		{
			if (assetsMap == null) throw new ArgumentNullException("assetsMap");
			_assetsMap = assetsMap;
		}

		protected DropGameContentManagerBase(
			IServiceProvider serviceProvider,
			string rootDirectory,
			IDictionary<string, string> assetsMap
			)
			: base(serviceProvider, rootDirectory)
		{
			if (assetsMap == null) throw new ArgumentNullException("assetsMap");
			_assetsMap = assetsMap;
		}

		public override T Load<T>(string assetName)
		{
			string path;
			if (_assetsMap.TryGetValue(assetName, out path))
			{
				assetName = path;
			}
			return base.Load<T>(assetName);
		}
	}
}
