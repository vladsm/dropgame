using Microsoft.Xna.Framework;

namespace DropGame
{
	internal abstract class ScreenLayoutBase
	{
		public IScalingTransformer Scaling { get; set; }

		protected ScreenLayoutBase(IScalingTransformer scaling)
		{
			Scaling = scaling;
		}

		protected Vector2 Adopt(Vector2 position)
		{
			return Scaling.Transform(position);
		}

		protected Vector2 Adopt(float x, float y)
		{
			return Scaling.Transform(new Vector2(x, y));
		}
	}
}
