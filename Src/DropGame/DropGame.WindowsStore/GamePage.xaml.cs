﻿using Windows.UI.Xaml;

using MonoGame.Framework;

namespace DropGame.WindowsStore
{
	/// <summary>
	/// The root page used to display the game.
	/// </summary>
	public sealed partial class GamePage
	{
		readonly DropGameImplementation _game;

		public GamePage(string launchArguments)
		{
			InitializeComponent();

			_game = XamlGame<DropGameImplementation>.Create(launchArguments, Window.Current.CoreWindow, this);
		}
	}
}
