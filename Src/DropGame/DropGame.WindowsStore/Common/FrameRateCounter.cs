﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace DropGame.WindowsStore
{
	public class FrameRateCounter : DrawableGameComponent
	{
		public int FrameRate { get; private set; }
		private int _frames;
		private double _seconds;
		private ReloadableResource<SpriteFont> _font;
		private SpriteBatch _spriteBatch;
		private Vector2 _outputPosition;

		public FrameRateCounter(Game game) : base(game)
		{
		}

		protected override void LoadContent()
		{
			_outputPosition = new Vector2(10, 10);
			_spriteBatch = new SpriteBatch(Game.GraphicsDevice);
			_font = Game.GetReloadableResource<SpriteFont>("Fonts/fps");
			base.LoadContent();
		}

		public override void Update(GameTime gameTime)
		{
			_seconds += gameTime.ElapsedGameTime.TotalSeconds;
			if (_seconds >= 1)
			{
				FrameRate = _frames;
				_seconds = 0;
				_frames = 0;
			}
			base.Update(gameTime);
		}

		public override void Draw(GameTime gameTime)
		{
			++_frames;

			_spriteBatch.Begin();
			_spriteBatch.DrawString(_font, "fps: " + FrameRate, _outputPosition, Color.White);
			_spriteBatch.End();

			base.Draw(gameTime);
		}
	}
}
